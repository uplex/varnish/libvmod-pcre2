/*-
 * Copyright 2017 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "vcl.h"
#include "cache/cache.h"
#include "vrt.h"
#include "vas.h"
#include "vdef.h"

#define PCRE2_CODE_UNIT_WIDTH 8
#include "pcre2.h"

#define ERR(ctx, msg) \
        errmsg((ctx), "vmod pcre2 error: " msg)

#define VERR(ctx, fmt, ...) \
        errmsg((ctx), "vmod pcre2 error: " fmt, __VA_ARGS__)

#define VERRNOMEM(ctx, fmt, ...) \
        VERR((ctx), fmt ", out of space", __VA_ARGS__)

#define ERRNOMEM(ctx, msg) \
        ERR((ctx), msg ", out of space")

#define COMPILE_OPTS							\
	VCL_BOOL allow_empty_class, VCL_BOOL anchored, VCL_ENUM bsrs,	\
		VCL_BOOL alt_bsux, VCL_BOOL alt_circumflex,		\
		VCL_BOOL alt_verbnames, VCL_BOOL caseless,		\
		VCL_BOOL dollar_endonly, VCL_BOOL dotall,		\
		VCL_BOOL dupnames, VCL_BOOL extended, VCL_BOOL firstline, \
		VCL_STRING locale, VCL_BOOL match_unset_backref,	\
		VCL_INT max_pattern_length, VCL_BOOL multiline,		\
		VCL_BOOL never_backslash_c, VCL_BOOL never_ucp,		\
		VCL_BOOL never_utf, VCL_ENUM newlines,			\
		VCL_BOOL no_auto_capture, VCL_BOOL no_auto_possess,	\
		VCL_BOOL no_dotstar_anchor, VCL_BOOL no_start_optimize, \
		VCL_BOOL no_utf_check, VCL_INT parens_nest_limit,	\
		VCL_BOOL ucp, VCL_BOOL ungreedy,			\
		VCL_BOOL use_offset_limit, VCL_BOOL utf

#define COMPILE_CTX_OPTS						\
	VCL_ENUM bsrs, VCL_STRING locale, VCL_INT max_pattern_length,	\
		VCL_ENUM newlines, VCL_INT parens_nest_limit

#define COMPILE_CTX_PARAMS						\
	bsrs, locale, max_pattern_length, newlines, parens_nest_limit

#define COMPILE_FLAGS							\
	VCL_BOOL allow_empty_class, VCL_BOOL anchored, VCL_BOOL alt_bsux, \
		VCL_BOOL alt_circumflex, VCL_BOOL alt_verbnames,	\
		VCL_BOOL caseless, VCL_BOOL dollar_endonly,		\
		VCL_BOOL dotall, VCL_BOOL dupnames, VCL_BOOL extended,	\
		VCL_BOOL firstline, VCL_BOOL match_unset_backref,	\
		VCL_BOOL multiline, VCL_BOOL never_backslash_c,		\
		VCL_BOOL never_ucp, VCL_BOOL never_utf,			\
		VCL_BOOL no_auto_capture, VCL_BOOL no_auto_possess,	\
		VCL_BOOL no_dotstar_anchor, VCL_BOOL no_start_optimize, \
		VCL_BOOL no_utf_check, VCL_BOOL ucp, VCL_BOOL ungreedy,	\
		VCL_BOOL use_offset_limit, VCL_BOOL utf

#define COMPILE_FLAGS_PARAMS						\
	allow_empty_class, anchored, alt_bsux, alt_circumflex,		\
		alt_verbnames, caseless, dollar_endonly, dotall,	\
		dupnames, extended, firstline, match_unset_backref,	\
		multiline, never_backslash_c, never_ucp, never_utf,	\
		no_auto_capture, no_auto_possess, no_dotstar_anchor,	\
		no_start_optimize, no_utf_check, ucp, ungreedy,		\
		use_offset_limit, utf

#define MATCH_OPTS							\
	VCL_INT len, VCL_BOOL anchored, VCL_INT match_limit,		\
		VCL_INT offset_limit, VCL_BOOL notbol, VCL_BOOL noteol, \
		VCL_BOOL notempty, VCL_BOOL notempty_atstart,		\
		VCL_BOOL no_jit, VCL_BOOL no_utf_check,			\
		VCL_INT recursion_limit

/* Doesn't repeat the anchored and no_utf_check options */
#define MATCHF_OPTS							\
	VCL_INT len, VCL_INT match_limit, VCL_INT offset_limit,		\
		VCL_BOOL notbol, VCL_BOOL noteol, VCL_BOOL notempty,	\
		VCL_BOOL notempty_atstart, VCL_BOOL no_jit,		\
		VCL_INT recursion_limit

#define MATCH_CTX_OPTS \
	VCL_INT match_limit, VCL_INT offset_limit, VCL_INT recursion_limit

#define MATCH_CTX_PARAMS \
	match_limit, offset_limit, recursion_limit

#define MATCH_FLAGS							\
	VCL_BOOL anchored, VCL_BOOL notbol, VCL_BOOL noteol,		\
		VCL_BOOL notempty, VCL_BOOL notempty_atstart,		\
		VCL_BOOL no_jit, VCL_BOOL no_utf_check

#define MATCH_SUB_FLAGS_PARAMS						\
	anchored, notbol, noteol, notempty, notempty_atstart, no_jit,	\
		no_utf_check, suball, sub_extended, unknown_unset,	\
		unset_empty

#define MATCH_FLAGS_PARAMS						\
	anchored, notbol, noteol, notempty, notempty_atstart, no_jit,	\
		no_utf_check, 0, 0, 0, 0

#define SUB_OPTS							\
	VCL_BOOL suball, VCL_BOOL sub_extended, VCL_BOOL unknown_unset, \
		VCL_BOOL unset_empty

struct vmod_pcre2_regex {
	unsigned	magic;
#define VMOD_PCRE2_REGEX_MAGIC 0x3adb2a78
	pcre2_code	*code;
	char		*vcl_name;
};

/*
 * PRIV_CALL scope. The match context and options are used by the match
 * method and function. The compile context and options are used by the
 * match and sub functions.
 */
struct match_call {
	unsigned		magic;
#define VMOD_PCRE2_MATCH_CALL_MAGIC 0x60e5bd33
	pcre2_match_context	*mctx;
	pcre2_compile_context	*cctx;
	uint32_t		match_options;
	uint32_t		compile_options;
};

void errmsg(VRT_CTX, const char *fmt, ...);

void report_pcre2_err(VRT_CTX, int errcode, const char * const restrict msg,
		      const char * const restrict post);

pcre2_compile_context *get_compile_opts(VRT_CTX, COMPILE_CTX_OPTS,
					COMPILE_FLAGS, uint32_t *options,
					const char * restrict const context,
					const char * restrict const caller);

struct match_call *get_match_opts(VRT_CTX, struct vmod_priv *priv,
				  MATCH_CTX_OPTS, MATCH_FLAGS, SUB_OPTS,
				  const char *context, const char *caller);

static inline int
check_uint32_range(VRT_CTX, long long limit, const char * const restrict name,
		   const char * const restrict context,
		   const char * const restrict caller)
{
	if (limit < 0 || limit > UINT32_MAX) {
		VERR(ctx, "%s (%lld) out of range in %s%s (must be >= 0 and "
		     "<= %" PRIu32 ")", name, limit, context, caller,
		     UINT32_MAX);
		return 0;
	}
	return 1;
}
