# -*-mode: vcl; coding: raw-text -*-

# For the newline test with the match function in
# compile_opts.vtc. Loaded via include, since vcl.inline, which is
# used by vtc, apparently changes \r characters to \n.
sub vcl_synth {
	set resp.http.f1-1
	    = pcre2.match("^abc", newline=LF, multiline=true, subject={"xyz
abc"});
	set resp.http.f1-2
	    = pcre2.match("^abc", newline=LF, multiline=true, subject={"xyz
abc"});
	set resp.http.f1-3
	    = pcre2.match("^abc", newline=LF, multiline=true,
	                  subject={"xyzabc"});
	set resp.http.f1-4
	    = pcre2.match("^abc", newline=LF, multiline=true,
	                  subject={"xyzabc"});
	set resp.http.f1-5
	    = pcre2.match("^abc", newline=LF, multiline=true,
	                  subject={"xyzabcpqr"});
	set resp.http.f1-6
	    = pcre2.match("^abc", newline=LF, multiline=true,
	                  subject={"xyzabc
"});
	set resp.http.f1-7
	    = pcre2.match("^abc", newline=LF, multiline=true,
	                  subject={"xyzabc
pqr"});
	set resp.http.f2-1
	    = pcre2.match("^abc", newline=CRLF, multiline=true, subject={"xyz
abclf>"});
	set resp.http.f2-2
	    = pcre2.match("^abc", newline=CRLF, multiline=true, subject={"xyz
abclf"});
	set resp.http.f2-3
	    = pcre2.match("^abc", newline=CRLF, multiline=true,
	                  subject={"xyzabclf"});
	set resp.http.f3-1
	    = pcre2.match("^abc", newline=CR, multiline=true,
	                  subject={"xyzabc"});
	set resp.http.f3-2
	    = pcre2.match("^abc", newline=CR, multiline=true, subject={"xyz
abc"});
	set resp.http.f3-3
	    = pcre2.match("^abc", newline=CR, multiline=true, subject={"xyz
abc"});
	set resp.http.f4-1 = pcre2.match("^a.b", newline=LF, subject={"ab"});
	set resp.http.f4-2 = pcre2.match("^a.b", newline=LF, subject={"a
b"});
	set resp.http.f5-1 = pcre2.match("^a.b", newline=CR, subject={"a
b"});
	set resp.http.f5-2 = pcre2.match("^a.b", newline=CR, subject={"ab"});
	set resp.http.f6-1
	    = pcre2.match("^a.b", newline=ANYCRLF, subject={"a�b"});
	set resp.http.f6-2
	    = pcre2.match("^a.b", newline=ANYCRLF, subject={"ab"});
	set resp.http.f7-1 = pcre2.match("^a.b", newline=ANY, subject={"a
b"});
	set resp.http.f7-2 = pcre2.match("^a.b", newline=ANY, subject={"ab"});
	set resp.http.f7-3
	    = pcre2.match("^a.b", newline=ANY, subject={"a�b"});
}
