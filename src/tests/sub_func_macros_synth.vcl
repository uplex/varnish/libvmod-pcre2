# vcl_synth for a test in sub.vtc that requires use of the ${...}
# substiution syntax. These cannot be used directly in a vtc, since
# varnishtest takes them for unknown macros and rejects the test.

sub vcl_synth {
	set resp.http.r1-1 = pcre2.sub("a(b)c(d)e", {""abcde""}, "X$1Y${2}Z");
	set resp.http.r1-2
	    = pcre2.sub("a(b)c(d)e", {""abcde-abcde""}, "X$1Y${2}Z",
	                suball=true);
	set resp.http.r2-1
	    = pcre2.sub("a(?<ONE>b)c(?<TWO>d)e", {""abcde""}, "X$ONE+${TWO}Z");
	set resp.http.r2-2
	    = pcre2.sub("a(?<ONE>b)c(?<TWO>d)e", {""abcde-abcde-""},
	                "X$ONE+${TWO}Z", suball=true);
	set resp.http.r3-1 = pcre2.sub(
	    "(*:pear)apple|(*:orange)lemon|(*:strawberry)blackberry",
	    "apple lemon blackberry", "${*MARK}", suball=true);
	set resp.http.r3-2 = pcre2.sub(
	    "(*:pear)apple|(*:orange)lemon|(*:strawberry)blackberry",
	    "apple strudel", "${*MARK}", suball=true);
	set resp.http.r3-3 = pcre2.sub(
	    "(*:pear)apple|(*:orange)lemon|(*:strawberry)blackberry",
	    "fruitless", "${*MARK}", suball=true);
	set resp.http.r3-4 = pcre2.sub(
	    "(*:pear)apple|(*:orange)lemon|(*:strawberry)blackberry",
	    "apple lemon blackberry", "${*MARK} sauce");
	set resp.http.r4-1
	    = pcre2.sub("abc", "123abc",
	                "a${A234567890123456789_123456789012}z");
	set resp.http.r4-2
	    = pcre2.sub("abc", "123abc",
	                "a${A23456789012345678901234567890123}z");
	set resp.http.r4-3 = pcre2.sub("abc", "123abc", "a${bcd");
	set resp.http.r4-4 = pcre2.sub("abc", "123abc", "a${b+d}z");
	set resp.http.r5-1
	    = pcre2.sub("a(?:(b)|(c))", "ab", "X${1:+1:-1}X${2:+2:-2}",
	                sub_extended=true);
	set resp.http.r5-2
	    = pcre2.sub("a(?:(b)|(c))", "ac", "X${1:+1:-1}X${2:+2:-2}",
	                sub_extended=true);
	set resp.http.r5-3
	    = pcre2.sub("a(?:(b)|(c))", "ab", "${1:+$1\:$1:$2}",
	                sub_extended=true);
	set resp.http.r5-4
	    = pcre2.sub("a(?:(b)|(c))", "ac", "${1:+$1\:$1:$2}",
	                sub_extended=true);
	set resp.http.r5-5
	    = pcre2.sub("a(?:(b)|(c))", "ab", "X${1:-1:-1}X${2:-2:-2}",
	                sub_extended=true);
	set resp.http.r5-6
	    = pcre2.sub("a(?:(b)|(c))", "ac", "X${1:-1:-1}X${2:-2:-2}",
	                sub_extended=true);
	set resp.http.r6
	    = pcre2.sub("(a)", "a", ">${1:+\Q$1:{}$$\E+\U$1}<",
	                sub_extended=true);
	set resp.http.r7-1
	    = pcre2.sub("X(b)Y", "XbY", "x${1:+$1\U$1}y", sub_extended=true);
	set resp.http.r7-2
	    = pcre2.sub("X(b)Y", "XbY", "\Ux${1:+$1$1}y", sub_extended=true);
	set resp.http.r8
	    = pcre2.sub("(a)", "a", "${*MARK:+a:b}", sub_extended=true);
	set resp.http.r9
	    = pcre2.sub("(abcd)", "abcd", "${1:+xy\kz}", sub_extended=true);
	set resp.http.r10-1
	    = pcre2.sub("abcd", "abcd", ">$1<", sub_extended=true);
	set resp.http.r10-2
	    = pcre2.sub("abcd", "abcd", ">xxx${xyz}<<<", sub_extended=true);
	set resp.http.r11-1
	    = pcre2.sub("a|(b)c", "cat", ">${2:-xx}<", sub_extended=true);
	set resp.http.r11-2
	    = pcre2.sub("a|(b)c", "cat", ">${2:-xx}<", sub_extended=true,
	                unknown_unset=true);
	set resp.http.r11-3
	    = pcre2.sub("a|(b)c", "cat", ">${X:-xx}<", sub_extended=true,
	                unknown_unset=true);
}
