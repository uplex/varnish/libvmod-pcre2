# -*-mode: vcl; coding: raw-text -*-

# For the bsr=ANYCRLF test in compile_opts.vtc. Loaded via include,
# since vcl.inline, which is used by vtc, apparently changes \r
# characters to \n.
sub vcl_synth {
	set resp.http.r1-1 = r1.match({"ab"});
	set resp.http.r1-2 = r1.match({"a
b"});
	set resp.http.r1-3 = r1.match({"a
b"});
	set resp.http.r1-4 = r1.match({"a�b"});
	set resp.http.r1-5 = r1.match({"ab"});
	set resp.http.r2-1 = r2.match({"a

b"});
	set resp.http.r2-2 = r2.match({"a
b"});
	set resp.http.r2-3 = r2.match({"a



b"});
	set resp.http.r2-4 = r2.match({"a��b"});
	set resp.http.r2-5 = r2.match({"ab"});

	set resp.http.f1-1
	    = pcre2.match("a\R?b", bsr=ANYCRLF, subject={"ab"});
	set resp.http.f1-2 = pcre2.match("a\R?b", bsr=ANYCRLF, subject={"a
b"});
	set resp.http.f1-3 = pcre2.match("a\R?b", bsr=ANYCRLF, subject={"a
b"});
	set resp.http.f1-4
	    = pcre2.match("a\R?b", bsr=ANYCRLF, subject={"a�b"});
	set resp.http.f1-5
	    = pcre2.match("a\R?b", bsr=ANYCRLF, subject={"ab"});
	set resp.http.f2-1 = pcre2.match("a\R{2,4}b", bsr=ANYCRLF, subject={"a

b"});
	set resp.http.f2-2 = pcre2.match("a\R{2,4}b", bsr=ANYCRLF, subject={"a
b"});
	set resp.http.f2-3 = pcre2.match("a\R{2,4}b", bsr=ANYCRLF, subject={"a



b"});
	set resp.http.f2-4
	    = pcre2.match("a\R{2,4}b", bsr=ANYCRLF, subject={"a��b"});
	set resp.http.f2-5
	    = pcre2.match("a\R{2,4}b", bsr=ANYCRLF, subject={"ab"});
}
