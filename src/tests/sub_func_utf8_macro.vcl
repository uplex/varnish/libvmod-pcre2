# This line is included from sub_utf8.vtc.yes, run as sub_utf8.vtc
# when Unicode is enabled for libpcre2. The extended syntax for pcre2
# substitutions includes the form ${...}, which varnishtest always
# interprets as a macro in a vtc script. So this snippet is included
# as a workaround.
set resp.http.r5 = pcre2.sub("((?<digit>\d)|(?<letter>\p{L}))", "ab12cde",
                             suball=true, utf=true, replacement=
		"<${digit:+digit; :not digit; }${letter:+letter:not a letter}>",
		    	     sub_extended=true);
