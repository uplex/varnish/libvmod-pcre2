# vcl_synth for a test in sub.vtc that requires use of the ${...}
# substiution syntax. These cannot be used directly in a vtc, since
# varnishtest takes them for unknown macros and rejects the test.

sub vcl_synth {
	set resp.http.r1-1 = r1.sub({""abcde""}, "X$1Y${2}Z");
	set resp.http.r1-2
	    = r1.sub({""abcde-abcde""}, "X$1Y${2}Z", suball=true);
	set resp.http.r2-1 = r2.sub({""abcde""}, "X$ONE+${TWO}Z");
	set resp.http.r2-2
	    = r2.sub({""abcde-abcde-""}, "X$ONE+${TWO}Z", suball=true);
	set resp.http.r3-1
	    = r3.sub("apple lemon blackberry", "${*MARK}", suball=true);
	set resp.http.r3-2 = r3.sub("apple strudel", "${*MARK}", suball=true);
	set resp.http.r3-3 = r3.sub("fruitless", "${*MARK}", suball=true);
	set resp.http.r3-4 = r3.sub("apple lemon blackberry", "${*MARK} sauce");
	set resp.http.r4-1
	    = r4.sub("123abc", "a${A234567890123456789_123456789012}z");
	set resp.http.r4-2
	    = r4.sub("123abc", "a${A23456789012345678901234567890123}z");
	set resp.http.r4-3 = r4.sub("123abc", "a${bcd");
	set resp.http.r4-4 = r4.sub("123abc", "a${b+d}z");
	set resp.http.r5-1
	    = r5.sub("ab", "X${1:+1:-1}X${2:+2:-2}", sub_extended=true);
	set resp.http.r5-2
	    = r5.sub("ac", "X${1:+1:-1}X${2:+2:-2}", sub_extended=true);
	set resp.http.r5-3 = r5.sub("ab", "${1:+$1\:$1:$2}", sub_extended=true);
	set resp.http.r5-4 = r5.sub("ac", "${1:+$1\:$1:$2}", sub_extended=true);
	set resp.http.r5-5
	    = r5.sub("ab", "X${1:-1:-1}X${2:-2:-2}", sub_extended=true);
	set resp.http.r5-6
	    = r5.sub("ac", "X${1:-1:-1}X${2:-2:-2}", sub_extended=true);
	set resp.http.r6
	    = r6.sub("a", ">${1:+\Q$1:{}$$\E+\U$1}<", sub_extended=true);
	set resp.http.r7-1 = r7.sub("XbY", "x${1:+$1\U$1}y", sub_extended=true);
	set resp.http.r7-2 = r7.sub("XbY", "\Ux${1:+$1$1}y", sub_extended=true);
	set resp.http.r8 = r8.sub("a", "${*MARK:+a:b}", sub_extended=true);
	set resp.http.r9 = r9.sub("abcd", "${1:+xy\kz}", sub_extended=true);
	set resp.http.r10-1 = r10.sub("abcd", ">$1<", sub_extended=true);
	set resp.http.r10-2
	    = r10.sub("abcd", ">xxx${xyz}<<<", sub_extended=true);
	set resp.http.r11-1 = r11.sub("cat", ">${2:-xx}<", sub_extended=true);
	set resp.http.r11-2 = r11.sub("cat", ">${2:-xx}<", sub_extended=true,
	                              unknown_unset=true);
	set resp.http.r11-3 = r11.sub("cat", ">${X:-xx}<", sub_extended=true,
	                              unknown_unset=true);
}
