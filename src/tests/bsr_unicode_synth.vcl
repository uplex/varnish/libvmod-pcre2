# -*-mode: vcl; coding: raw-text -*-

# For the bsr=UNICODE test in compile_opts.vtc. Loaded via include,
# since vcl.inline, which is used by vtc, apparently changes \r
# characters to \n.
sub vcl_synth {
	set resp.http.r1-1 = r1.match({"a
b"});
	set resp.http.r1-2 = r1.match({"ab"});
	set resp.http.r1-3 = r1.match({"a
b"});
	set resp.http.r1-4 = r1.match({"ab"});
	set resp.http.r1-5 = r1.match({"ab"});
	set resp.http.r1-6 = r1.match({"a�b"});
	set resp.http.r1-7 = r1.match({"a
b"});
	set resp.http.r2-1 = r2.match("ab");
	set resp.http.r2-2 = r2.match({"a
b"});
	set resp.http.r2-3 = r2.match({"ab"});
	set resp.http.r2-4 = r2.match({"a
b"});
	set resp.http.r2-5 = r2.match({"ab"});
	set resp.http.r2-6 = r2.match({"ab"});
	set resp.http.r2-7 = r2.match({"a�b"});
	set resp.http.r2-8 = r2.match({"a
b"});
	set resp.http.r2-9 = r2.match({"a
�b"});
	set resp.http.r3-1 = r3.match({"a
b"});
	set resp.http.r3-2 = r3.match({"ab"});
	set resp.http.r3-3 = r3.match({"a
b"});
	set resp.http.r3-4 = r3.match({"ab"});
	set resp.http.r3-5 = r3.match({"ab"});
	set resp.http.r3-6 = r3.match({"a�b"});
	set resp.http.r3-7 = r3.match({"a
b"});
	set resp.http.r3-8 = r3.match({"a
�b"});
	set resp.http.r3-9 = r3.match("ab");
	set resp.http.r4-1 = r4.match({"a
b"});
	set resp.http.r4-2 = r4.match({"a
b"});
	set resp.http.r4-3 = r4.match({"a
�b"});
	set resp.http.r4-4 = r4.match({"a

b"});
	set resp.http.r4-5 = r4.match({"a


b"});
	set resp.http.r4-6 = r4.match({"a

b"});
	set resp.http.r4-7 = r4.match({"a


b"});
	set resp.http.r4-8 = r4.match({"a


b"});
	set resp.http.r4-9 = r4.match({"a"});
}
