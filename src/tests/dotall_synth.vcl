# -*-mode: vcl; coding: raw-text -*-

# For the dotall test in compile_opts.vtc. Loaded via include, since
# vcl.inline, which is used by vtc, apparently changes \r characters
# to \n.
sub vcl_synth {
	set resp.http.r1-1 = r1.match({"12
34"});
	set resp.http.r2-1 = r2.match({"12
34"});
	set resp.http.r1-2 = r1.match({"1234"});
	set resp.http.r2-2 = r2.match({"1234"});
	set resp.http.r3 = r3.match({"abc
def"});
	set resp.http.r4 = r4.match({"abc
def"});

	set resp.http.f1-1 = pcre2.match("^12.34", dotall=true, subject={"12
34"});
	set resp.http.f2-1 = pcre2.match("^12.34", {"12
34"});
	set resp.http.f1-2
	    = pcre2.match("^12.34", dotall=true, subject={"1234"});
	set resp.http.f2-2 = pcre2.match("^12.34", {"1234"});
	set resp.http.f3 = pcre2.match("\A.*\Z", dotall=true, subject={"abc
def"});
	set resp.http.f4 = pcre2.match("\A.*\Z", {"abc
def"});
}
