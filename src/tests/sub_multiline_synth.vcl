# vcl_synth for a test in sub.vtc that requires CR's in the subject
# string, because vcl.inline called during varnishtest replaces them
# with "\r" (slash followed by 'r').
sub vcl_synth {
	if (req.http.test == "1") {
		set resp.body
		    = r1.sub(suball=true, replacement="-", subject={"X

Y"});
	}
	elsif (req.http.test == "2") {
		set resp.body
		    = r2.sub(suball=true, replacement="-", subject={"X

Y"});
	}
	elsif (req.http.test == "3") {
		set resp.body
		    = r3.sub(suball=true, replacement="-", subject={"X

Y"});
	}
	elsif (req.http.test == "4") {
		set resp.body
		    = r4.sub(suball=true, replacement="NaN", subject={"15
foo
20
bar
baz

20"});
	}
	else {
		set resp.status = 500;
	}
	return(deliver);
}
