# -*-mode: vcl; coding: raw-text -*-

# For the bsr=UNICODE test in compile_opts.vtc, to test the match
# function. Loaded via include, since vcl.inline, which is used by
# vtc, apparently changes \r characters to \n.
sub vcl_synth {
	set resp.http.f1-1 = pcre2.match("^a\Rb", bsr=UNICODE, subject={"a
b"});
	set resp.http.f1-2
	    = pcre2.match("^a\Rb", bsr=UNICODE, subject={"ab"});
	set resp.http.f1-3 = pcre2.match("^a\Rb", bsr=UNICODE, subject={"a
b"});
	set resp.http.f1-4
	    = pcre2.match("^a\Rb", bsr=UNICODE, subject={"ab"});
	set resp.http.f1-5
	    = pcre2.match("^a\Rb", bsr=UNICODE, subject={"ab"});
	set resp.http.f1-6
	    = pcre2.match("^a\Rb", bsr=UNICODE, subject={"a�b"});
	set resp.http.f1-7 = pcre2.match("^a\Rb", bsr=UNICODE, subject={"a
b"});
	set resp.http.f2-1 = pcre2.match("^a\R*b", bsr=UNICODE, subject="ab");
	set resp.http.f2-2 = pcre2.match("^a\R*b", bsr=UNICODE, subject={"a
b"});
	set resp.http.f2-3
	    = pcre2.match("^a\R*b", bsr=UNICODE, subject={"ab"});
	set resp.http.f2-4 = pcre2.match("^a\R*b", bsr=UNICODE, subject={"a
b"});
	set resp.http.f2-5
	    = pcre2.match("^a\R*b", bsr=UNICODE, subject={"ab"});
	set resp.http.f2-6
	    = pcre2.match("^a\R*b", bsr=UNICODE, subject={"ab"});
	set resp.http.f2-7
	    = pcre2.match("^a\R*b", bsr=UNICODE, subject={"a�b"});
	set resp.http.f2-8 = pcre2.match("^a\R*b", bsr=UNICODE, subject={"a
b"});
	set resp.http.f2-9 = pcre2.match("^a\R*b", bsr=UNICODE, subject={"a
�b"});
	set resp.http.f3-1 = pcre2.match("^a\R+b", bsr=UNICODE, subject={"a
b"});
	set resp.http.f3-2
	    = pcre2.match("^a\R+b", bsr=UNICODE, subject={"ab"});
	set resp.http.f3-3 = pcre2.match("^a\R+b", bsr=UNICODE, subject={"a
b"});
	set resp.http.f3-4
	    = pcre2.match("^a\R+b", bsr=UNICODE, subject={"ab"});
	set resp.http.f3-5
	    = pcre2.match("^a\R+b", bsr=UNICODE, subject={"ab"});
	set resp.http.f3-6
	    = pcre2.match("^a\R+b", bsr=UNICODE, subject={"a�b"});
	set resp.http.f3-7 = pcre2.match("^a\R+b", bsr=UNICODE, subject={"a
b"});
	set resp.http.f3-8 = pcre2.match("^a\R+b", bsr=UNICODE, subject={"a
�b"});
	set resp.http.f3-9 = pcre2.match("^a\R+b", bsr=UNICODE, subject="ab");
	set resp.http.f4-1 = pcre2.match("^a\R{1,3}b", bsr=UNICODE, subject={"a
b"});
	set resp.http.f4-2 = pcre2.match("^a\R{1,3}b", bsr=UNICODE, subject={"a
b"});
	set resp.http.f4-3 = pcre2.match("^a\R{1,3}b", bsr=UNICODE, subject={"a
�b"});
	set resp.http.f4-4
	    = pcre2.match("^a\R{1,3}b", bsr=UNICODE, subject={"a

b"});
	set resp.http.f4-5
	    = pcre2.match("^a\R{1,3}b", bsr=UNICODE, subject={"a


b"});
	set resp.http.f4-6 = pcre2.match("^a\R{1,3}b", bsr=UNICODE, subject={"a

b"});
	set resp.http.f4-7 = pcre2.match("^a\R{1,3}b", bsr=UNICODE, subject={"a


b"});
	set resp.http.f4-8 = pcre2.match("^a\R{1,3}b", bsr=UNICODE, subject={"a


b"});
	set resp.http.f4-9
	    = pcre2.match("^a\R{1,3}b", bsr=UNICODE, subject={"a"});
}
