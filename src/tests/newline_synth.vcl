# -*-mode: vcl; coding: raw-text -*-

# For the newline test with the match method in
# compile_opts.vtc. Loaded via include, since vcl.inline, which is
# used by vtc, apparently changes \r characters to \n.
sub vcl_synth {
	set resp.http.r1-1 = r1.match({"xyz
abc"});
	set resp.http.r1-2 = r1.match({"xyz
abc"});
	set resp.http.r1-3 = r1.match({"xyzabc"});
	set resp.http.r1-4 = r1.match({"xyzabc"});
	set resp.http.r1-5 = r1.match({"xyzabcpqr"});
	set resp.http.r1-6 = r1.match({"xyzabc
"});
	set resp.http.r1-7 = r1.match({"xyzabc
pqr"});
	set resp.http.r2-1 = r2.match({"xyz
abclf>"});
	set resp.http.r2-2 = r2.match({"xyz
abclf"});
	set resp.http.r2-3 = r2.match({"xyzabclf"});
	set resp.http.r3-1 = r3.match({"xyzabc"});
	set resp.http.r3-2 = r3.match({"xyz
abc"});
	set resp.http.r3-3 = r3.match({"xyz
abc"});
	set resp.http.r4-1 = r4.match({"ab"});
	set resp.http.r4-2 = r4.match({"a
b"});
	set resp.http.r5-1 = r5.match({"a
b"});
	set resp.http.r5-2 = r5.match({"ab"});
	set resp.http.r6-1 = r6.match({"a�b"});
	set resp.http.r6-2 = r6.match({"ab"});
	set resp.http.r7-1 = r7.match({"a
b"});
	set resp.http.r7-2 = r7.match({"ab"});
	set resp.http.r7-3 = r7.match({"a�b"});
}
