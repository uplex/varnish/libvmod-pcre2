/*-
 * Copyright 2017 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "vmod_pcre2.h"

#include "vcc_if.h"

VCL_BOOL
vmod_config_bool(VRT_CTX, VCL_ENUM configs)
{
	uint32_t what, where;
	int ret;

	(void) ctx;
	
	if (strcmp(configs, "JIT") == 0)
		what = PCRE2_CONFIG_JIT;
	else if (strcmp(configs, "STACKRECURSE") == 0)
		what = PCRE2_CONFIG_STACKRECURSE;
	else if (strcmp(configs, "UNICODE") == 0)
		what = PCRE2_CONFIG_UNICODE;
	else
		WRONG("Illegal config enum in config_bool");

	ret = pcre2_config(what, &where);
	assert(ret >= 0);
	return (VCL_BOOL) where;
}

VCL_STRING
vmod_config_str(VRT_CTX, VCL_ENUM configs)
{
	uint32_t what;
	int ret;
	void *buf;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	if (strcmp(configs, "BSR") == 0)
		what = PCRE2_CONFIG_BSR;
	else if (strcmp(configs, "JITTARGET") == 0)
		what = PCRE2_CONFIG_JITTARGET;
	else if (strcmp(configs, "NEWLINE") == 0)
		what = PCRE2_CONFIG_NEWLINE;
	else if (strcmp(configs, "UNICODE_VERSION") == 0)
		what = PCRE2_CONFIG_UNICODE_VERSION;
	else if (strcmp(configs, "VERSION") == 0)
		what = PCRE2_CONFIG_VERSION;
	else
		WRONG("Illegal config enum in config_str");


	if (what == PCRE2_CONFIG_NEWLINE || what == PCRE2_CONFIG_BSR) {
		uint32_t where;

		ret = pcre2_config(what, &where);
		assert(ret >= 0);

		if (what == PCRE2_CONFIG_NEWLINE)
			switch(where) {
			case PCRE2_NEWLINE_CR:
				return "CR";
			case PCRE2_NEWLINE_LF:
				return "LF";
			case PCRE2_NEWLINE_CRLF:
				return "CRLF";
			case PCRE2_NEWLINE_ANY:
				return "ANY";
			case PCRE2_NEWLINE_ANYCRLF:
				return "ANYCRLF";
			default:
				WRONG("Illegal result for "
				      "PCRE2_CONFIG_NEWLINE");
			}

		assert (what == PCRE2_CONFIG_BSR);
		switch(where) {
		case PCRE2_BSR_UNICODE:
			return "UNICODE";
		case PCRE2_BSR_ANYCRLF:
			return "ANYCRLF";
		default:
			WRONG("Illegal result for PCRE2_CONFIG_BSR");
		}
	}

	ret = pcre2_config(what, NULL);
	if (what == PCRE2_CONFIG_JITTARGET && ret == PCRE2_ERROR_BADOPTION)
		return "JIT not supported";
	assert(ret > 0);
	if ((buf = WS_Alloc(ctx->ws, ret)) == NULL) {
		ERRNOMEM(ctx, "allocating result in pcre2.config_str()");
		return NULL;
	}
	ret = pcre2_config(what, buf);
	assert(ret > 0);
	return (VCL_STRING) buf;
}

VCL_INT
vmod_config_int(VRT_CTX, VCL_ENUM configs)
{
	uint32_t what, where;
	int ret;

	(void) ctx;

	if (strcmp(configs, "LINKSIZE") == 0)
		what = PCRE2_CONFIG_LINKSIZE;
	else if (strcmp(configs, "MATCHLIMIT") == 0)
		what = PCRE2_CONFIG_MATCHLIMIT;
	else if (strcmp(configs, "PARENSLIMIT") == 0)
		what = PCRE2_CONFIG_PARENSLIMIT;
	else if (strcmp(configs, "RECURSIONLIMIT") == 0)
		what = PCRE2_CONFIG_RECURSIONLIMIT;
	else
		WRONG("Illegal config enum in config_int");

	ret = pcre2_config(what, &where);
	assert(ret >= 0);
	return (VCL_INT) where;
}
