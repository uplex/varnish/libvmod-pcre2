/*-
 * Copyright 2017 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>

#include "vmod_pcre2.h"

#include "vcc_if.h"

VCL_BOOL
vmod_regex_info_bool(VRT_CTX, struct vmod_pcre2_regex *regex, VCL_ENUM bools,
		     VCL_BOOL compiled)
{
	uint32_t what, where, option, opts = PCRE2_INFO_ALLOPTIONS, codetype;
	int ret;

	(void) ctx;
	CHECK_OBJ_NOTNULL(regex, VMOD_PCRE2_REGEX_MAGIC);

	if (!compiled)
		opts = PCRE2_INFO_ARGOPTIONS;

	if (strcmp(bools, "ALLOW_EMPTY_CLASS") == 0) {
		what = opts;
		option = PCRE2_ALLOW_EMPTY_CLASS;
	}
	else if (strcmp(bools, "ANCHORED") == 0) {
		what = opts;
		option = PCRE2_ANCHORED;
	}
	else if (strcmp(bools, "ALT_BSUX") == 0) {
		what = opts;
		option = PCRE2_ANCHORED;
	}
	else if (strcmp(bools, "ALT_CIRCUMFLEX") == 0) {
		what = opts;
		option = PCRE2_ALT_CIRCUMFLEX;
	}
	else if (strcmp(bools, "ALT_VERBNAMES") == 0) {
		what = opts;
		option = PCRE2_ALT_VERBNAMES;
	}
	else if (strcmp(bools, "CASELESS") == 0) {
		what = opts;
		option = PCRE2_CASELESS;
	}
	else if (strcmp(bools, "DOLLAR_ENDONLY") == 0) {
		what = opts;
		option = PCRE2_DOLLAR_ENDONLY;
	}
	else if (strcmp(bools, "DOTALL") == 0) {
		what = opts;
		option = PCRE2_DOTALL;
	}
	else if (strcmp(bools, "DUPNAMES") == 0) {
		what = opts;
		option = PCRE2_DUPNAMES;
	}
	else if (strcmp(bools, "EXTENDED") == 0) {
		what = opts;
		option = PCRE2_EXTENDED;
	}
	else if (strcmp(bools, "FIRSTLINE") == 0) {
		what = opts;
		option = PCRE2_FIRSTLINE;
	}
	else if (strcmp(bools, "MATCH_UNSET_BACKREF") == 0) {
		what = opts;
		option = PCRE2_MATCH_UNSET_BACKREF;
	}
	else if (strcmp(bools, "MULTILINE") == 0) {
		what = opts;
		option = PCRE2_MULTILINE;
	}
	else if (strcmp(bools, "NEVER_BACKSLASH_C") == 0) {
		what = opts;
		option = PCRE2_NEVER_BACKSLASH_C;
	}
	else if (strcmp(bools, "NEVER_UCP") == 0) {
		what = opts;
		option = PCRE2_NEVER_UCP;
	}
	else if (strcmp(bools, "NEVER_UTF") == 0) {
		what = opts;
		option = PCRE2_NEVER_UTF;
	}
	else if (strcmp(bools, "NO_AUTO_CAPTURE") == 0) {
		what = opts;
		option = PCRE2_NO_AUTO_CAPTURE;
	}
	else if (strcmp(bools, "NO_AUTO_POSSESS") == 0) {
		what = opts;
		option = PCRE2_NO_AUTO_POSSESS;
	}
	else if (strcmp(bools, "NO_DOTSTAR_ANCHOR") == 0) {
		what = opts;
		option = PCRE2_NO_DOTSTAR_ANCHOR;
	}
	else if (strcmp(bools, "NO_START_OPTIMIZE") == 0) {
		what = opts;
		option = PCRE2_NO_START_OPTIMIZE;
	}
	else if (strcmp(bools, "NO_UTF_CHECK") == 0) {
		what = opts;
		option = PCRE2_NO_UTF_CHECK;
	}
	else if (strcmp(bools, "UCP") == 0) {
		what = opts;
		option = PCRE2_UCP;
	}
	else if (strcmp(bools, "UNGREEDY") == 0) {
		what = opts;
		option = PCRE2_UNGREEDY;
	}
	else if (strcmp(bools, "USE_OFFSET_LIMIT") == 0) {
		what = opts;
		option = PCRE2_USE_OFFSET_LIMIT;
	}
	else if (strcmp(bools, "UTF") == 0) {
		what = opts;
		option = PCRE2_UTF;
	}
	else if (strcmp(bools, "HAS_FIRSTCODEUNIT") == 0) {
		 what = PCRE2_INFO_FIRSTCODETYPE;
		 codetype = 1;
	}
	else if (strcmp(bools, "MATCH_ATSTART") == 0) {
		 what = PCRE2_INFO_FIRSTCODETYPE;
		 codetype = 2;
	}
	else if (strcmp(bools, "HAS_LASTCODEUNIT") == 0)
		 what = PCRE2_INFO_LASTCODETYPE;
	else if (strcmp(bools, "HAS_BACKSLASHC") == 0)
		 what = PCRE2_INFO_HASBACKSLASHC;
	else if (strcmp(bools, "HAS_CRORLF") == 0)
		 what = PCRE2_INFO_HASCRORLF;
	else if (strcmp(bools, "JCHANGED") == 0)
		 what = PCRE2_INFO_JCHANGED;
	else if (strcmp(bools, "MATCH_EMPTY") == 0)
		 what = PCRE2_INFO_MATCHEMPTY;
	else
		WRONG("illegal enum in info_int");

	ret = pcre2_pattern_info(regex->code, what, &where);
	AZ(ret);

	if (what == opts)
		return (where & option);
	if (what == PCRE2_INFO_FIRSTCODETYPE)
		return where == codetype;
	return where;
}

VCL_INT
vmod_regex_info_int(VRT_CTX, struct vmod_pcre2_regex *regex, VCL_ENUM ints)
{
	uint32_t what, where;
	size_t sz;
	int ret;

	(void) ctx;
	CHECK_OBJ_NOTNULL(regex, VMOD_PCRE2_REGEX_MAGIC);

	if (strcmp(ints, "BACKREFMAX") == 0)
		what = PCRE2_INFO_BACKREFMAX;
	else if (strcmp(ints, "CAPTURECOUNT") == 0)
		what = PCRE2_INFO_CAPTURECOUNT;
	else if (strcmp(ints, "JITSIZE") == 0)
		what = PCRE2_INFO_JITSIZE;
	else if (strcmp(ints, "MATCHLIMIT") == 0)
		what = PCRE2_INFO_MATCHLIMIT;
	else if (strcmp(ints, "MAXLOOKBEHIND") == 0)
		what = PCRE2_INFO_MAXLOOKBEHIND;
	else if (strcmp(ints, "MINLENGTH") == 0)
		what = PCRE2_INFO_MINLENGTH;
	else if (strcmp(ints, "RECURSIONLIMIT") == 0)
		what = PCRE2_INFO_RECURSIONLIMIT;
	else if (strcmp(ints, "SIZE") == 0)
		what = PCRE2_INFO_SIZE;
	else
		WRONG("illegal enum in info_int");

	if (what == PCRE2_INFO_JITSIZE || what == PCRE2_INFO_SIZE) {
		ret = pcre2_pattern_info(regex->code, what, &sz);
		AZ(ret);
		return sz;
	}

	ret = pcre2_pattern_info(regex->code, what, &where);
	if (ret == PCRE2_ERROR_UNSET) {
		assert(what == PCRE2_INFO_MATCHLIMIT
		       || what == PCRE2_INFO_RECURSIONLIMIT);
		return -1;
	}
	AZ(ret);
	return where;
}

VCL_STRING
vmod_regex_info_str(VRT_CTX, struct vmod_pcre2_regex *regex, VCL_ENUM strs,
		    VCL_STRING sep)
{
	uint32_t what, where;
	int ret;
	char *str;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(regex, VMOD_PCRE2_REGEX_MAGIC);
	if (sep == NULL) {
		VERR(ctx, "sep is undefined in %s.info_str()", regex->vcl_name);
		return NULL;
	}

	if (strcmp(strs, "BSR") == 0)
		what = PCRE2_INFO_BSR;
	else if (strcmp(strs, "FIRSTCODEUNIT") == 0)
		what = PCRE2_INFO_FIRSTCODEUNIT;
	else if (strcmp(strs, "FIRSTCODEUNITS") == 0)
		what = PCRE2_INFO_FIRSTBITMAP;
	else if (strcmp(strs, "LASTCODEUNIT") == 0)
		what = PCRE2_INFO_LASTCODEUNIT;
	else if (strcmp(strs, "NEWLINE") == 0)
		what = PCRE2_INFO_NEWLINE;
	else
		WRONG("illegal enum in info_str");

	if (what == PCRE2_INFO_FIRSTBITMAP) {
		unsigned bytes;
		int ctr, len;
		const uint8_t *map;
		char *p;

		ret = pcre2_pattern_info(regex->code, what, &map);
		AZ(ret);
		if (map == NULL)
			return "";
		str = WS_Front(ctx->ws);
		bytes = WS_Reserve(ctx->ws, 0);
		if (bytes == 0)
			goto nomem;
		ctr = bytes;
		len = strlen(sep);
		p = str;
		for (int i = 1; i < 256; i++)
			if (map[i >> 3] & (1 << (i & 7))) {
				if (ctr - len - 2 < 0)
					goto nomem;
				sprintf(p, "%c%s", (uint8_t)i, sep);
				p += len + 1;
				ctr -= len + 1;
			}
		assert(ctr >= 0);
		assert((unsigned)ctr < bytes);
		*(p - len) = '\0';
		WS_Release(ctx->ws, strlen(str) + 1);
		return (VCL_STRING)str;

	nomem:
		VERRNOMEM(ctx, "allocating space for first code units in "
			  "%s.info_str()", regex->vcl_name);
		WS_Release(ctx->ws, 0);
		return NULL;
	}

	ret = pcre2_pattern_info(regex->code, what, &where);
	AZ(ret);

	if (what == PCRE2_INFO_FIRSTCODEUNIT
	    || what == PCRE2_INFO_LASTCODEUNIT) {
		if (where == 0)
			return "";
		if ((str = WS_Printf(ctx->ws, "%c", where)) == NULL) {
			VERRNOMEM(ctx, "allocating space for return string in "
				  "%s.info_str()", regex->vcl_name);
			return NULL;
		}
		return (VCL_STRING)str;
	}

	if (what == PCRE2_INFO_BSR)
		switch(where) {
		case PCRE2_BSR_UNICODE:
			return("UNICODE");
		case PCRE2_BSR_ANYCRLF:
			return ("ANYCRLF");
		default:
			WRONG("unexpected value returned for BSR");
		}

	assert(what == PCRE2_INFO_NEWLINE);
	switch(where) {
		case PCRE2_NEWLINE_CR:
			return("CR");
		case PCRE2_NEWLINE_LF:
			return ("LF");
		case PCRE2_NEWLINE_CRLF:
			return ("CRLF");
		case PCRE2_NEWLINE_ANY:
			return ("ANY");
		case PCRE2_NEWLINE_ANYCRLF:
			return ("ANYCRLF");
		default:
			WRONG("unexpected value returned for BSR");
	}
}
