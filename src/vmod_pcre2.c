/*-
 * Copyright 2017 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* for strdup() */
#define _POSIX_C_SOURCE 200809L

#include "config.h"

#include "vmod_pcre2.h"

#include "vcc_if.h"

/*
 * PRIV_TASK scope. The general context is used by all methods and
 * functions.  The match_data is used by the match(), backref() and
 * namedref() functions.
 */
struct task {
	unsigned		magic;
#define VMOD_PCRE2_TASK_MAGIC 0x21000676
	pcre2_general_context	*gctx;
	pcre2_match_data	*mdata;
};

enum ref_e {
	NUMBERED = 0,
	NAMED = 1,
};

static int have_jit = 0;

/* Event function */

int __match_proto__(vmod_event_f)
event(VRT_CTX, struct vmod_priv *priv, enum vcl_event_e e)
{
	(void) ctx;
	(void) priv;

	if (e == VCL_EVENT_LOAD) {
		int ret;
		uint32_t where;

		/*
		 * A VCL_INT must never be larger than PCRE2_SIZE_MAX, so
		 * that some assignments from INT params cannot be out of
		 * range.
		 * XXX: C11 static_assert would catch this at compile
		 * time.
		 */
		assert(sizeof(VCL_INT) <= sizeof(PCRE2_SIZE));

		ret = pcre2_config(PCRE2_CONFIG_JIT, &where);
		assert(ret >= 0);
		have_jit = where;
	}
	return 0;
}

/* Object regex */

static void *
ws_malloc(PCRE2_SIZE sz, void *ws)
{
	return WS_Alloc(ws, sz);
}

static void
ws_free(void *ptr, void *ws)
{
	WS_Assert_Allocated(ws, ptr, 0);
}

static inline struct task *
get_task(VRT_CTX, struct vmod_priv *priv_task,
	 const char * const restrict context,
	 const char * const restrict caller)
{
	struct task *match_task;

	/*
	 * The general context is task-scoped and shared by all methods
	 * and functions from this VMOD in the task. It uses WS_Alloc as
	 * the custom allocator.
	 */
	if (priv_task->priv == NULL) {
		if ((match_task = WS_Alloc(ctx->ws, sizeof(*match_task)))
		    == NULL) {
			VERRNOMEM(ctx, "allocating task-scoped data in %s%s",
				  context, caller);
			return NULL;
		}
		if ((match_task->gctx
		     = pcre2_general_context_create(ws_malloc, ws_free,
						    ctx->ws)) == NULL) {
			VERRNOMEM(ctx, "initializing general context in %s%s",
				  context, caller);
			return NULL;
		}
		match_task->magic = VMOD_PCRE2_TASK_MAGIC;
		match_task->mdata = NULL;
		priv_task->priv = match_task;
		priv_task->len = sizeof(*match_task);
		priv_task->free = NULL;
	}
	else {
		WS_Assert_Allocated(ctx->ws, priv_task->priv,
				    sizeof(*match_task));
		CAST_OBJ(match_task, priv_task->priv, VMOD_PCRE2_TASK_MAGIC);
		/*
		 * pcre2_general_context is declared as an incomplete
		 * type, so we can't check if its full size is allocated.
		 */
		WS_Assert_Allocated(ctx->ws, match_task->gctx, 0);
	}
	return match_task;
}

static inline pcre2_code *
compile(VRT_CTX, pcre2_compile_context * restrict const cctx,
	VCL_STRING const restrict pattern, uint32_t options, int do_jit,
	const char * const restrict context, const char * const restrict caller)
{
	pcre2_code *code;
	int err_code = 0;
	PCRE2_SIZE err_offset;

	/* XXX set the length via parameter */
	code = pcre2_compile((PCRE2_SPTR)pattern, PCRE2_ZERO_TERMINATED,
			     options, &err_code, &err_offset, cctx);
	if (code == NULL) {
		char *msg, *offset_msg;
		uintptr_t snap = WS_Snapshot(ctx->ws);

		if ((msg = WS_Printf(ctx->ws, "Cannot compile '%s' in %s%s",
				     pattern, context, caller)) == NULL)
			msg = "";
		if ((offset_msg = WS_Printf(ctx->ws, " at offset %zu",
					    err_offset)) == NULL)
			offset_msg = "";
		report_pcre2_err(ctx, err_code, msg, offset_msg);
		WS_Reset(ctx->ws, snap);
		return NULL;
	}
	if (do_jit) {
		int ret;

		/* XXX check option compatibility; disable via param */
		/* XXX set complete or soft/hard partial via param */
		options |= PCRE2_JIT_COMPLETE;
		ret = pcre2_jit_compile(code, options);
		if (ret != 0) {
			char *msg;
			uintptr_t snap = WS_Snapshot(ctx->ws);

			if ((msg = WS_Printf(ctx->ws, "Cannot jit-compile "
					     "'%s' in %s%s", pattern, context,
					     caller)) == NULL)
				msg = "";
			report_pcre2_err(ctx, ret, msg, "");
			WS_Reset(ctx->ws, snap);
			return NULL;
		}
	}
	return code;
}

static inline VCL_BOOL
match(VRT_CTX, pcre2_code * restrict const code,
      VCL_STRING restrict const subject, VCL_INT len, const uint32_t options,
      pcre2_match_data * restrict const mdata,
      pcre2_match_context * restrict const mctx,
      const char * restrict const context, const char * restrict const caller)
{
	PCRE2_SPTR safe_subject;
	int ret;
	uintptr_t snap;
	char *msg;

	if (subject == NULL) {
		safe_subject = (PCRE2_SPTR)"";
		len = 0;
	}
	else if (len == 0)
		len = strlen(subject);
	if (subject != NULL) {
		if (WS_Inside(ctx->ws, subject, subject + len))
			safe_subject = (PCRE2_SPTR)subject;
		else if ((safe_subject = WS_Copy(ctx->ws,
						 (const void *) subject, len))
			 == NULL) {
			VERRNOMEM(ctx, "in %s%s: copying subject to workspace",
				  context, caller);
			return 0;
		}
	}

	/* XXX param for start_offset */
	ret = pcre2_match(code, safe_subject, (PCRE2_SIZE)len, 0, options,
			  mdata, mctx);
	if (ret == PCRE2_ERROR_NOMATCH)
		return 0;
	assert(ret != PCRE2_ERROR_PARTIAL); /* XXX */
	if (ret >= 0)
		return 1;

	snap = WS_Snapshot(ctx->ws);
	if ((msg = WS_Printf(ctx->ws, "in %s%s", context, caller)) == NULL)
		msg = "";
	report_pcre2_err(ctx, ret, msg, "");
	WS_Reset(ctx->ws, snap);
	return 0;
}

static inline VCL_STRING
refer(VRT_CTX, VCL_INT ref, VCL_STRING restrict const name,
      VCL_STRING restrict const fallback, const enum ref_e reftype,
      pcre2_match_data *mdata, const char * restrict const context,
      const char * restrict const caller,
      const char * restrict const default_fallback)
{

	PCRE2_UCHAR *buf;
	PCRE2_SIZE len;
	int ret = PCRE2_ERROR_UNAVAILABLE;
	char *msg = NULL;
	uintptr_t snap;

	if (fallback == NULL) {
		VERR(ctx, "in %s.%s(): fallback is undefined", context, caller);
		return default_fallback;
	}
	if (mdata == NULL) {
		VERR(ctx, "%s.%s() called without prior match", context,
		     caller);
		return fallback;
	}
	WS_Assert_Allocated(ctx->ws, mdata, 0);

	switch (reftype) {
	case NUMBERED:
		if (!check_uint32_range(ctx, ref, "ref", context, ".backref()"))
			return fallback;
		ret = pcre2_substring_get_bynumber(mdata, (uint32_t)ref, &buf,
						   &len);
		break;
	case NAMED:
		if (name == NULL) {
			VERR(ctx, "in %s.%s(<undefined>): name is undefined",
			     context, caller);
			return fallback;
		}
		ret = pcre2_substring_get_byname(mdata, (PCRE2_SPTR)name, &buf,
						 &len);
	}

	if (ret == 0) {
		WS_Assert_Allocated(ctx->ws, buf, len + 1);
		return (VCL_STRING)buf;
	}

	/*
	 * This error is returned when the ovector was too small, cannot
	 * happen after using pcre2_match_data_create_from_pattern() to
	 * get the match data block.
	 */
	assert(ret != PCRE2_ERROR_UNAVAILABLE);

	snap = WS_Snapshot(ctx->ws);
	if ((msg = WS_Printf(ctx->ws, "in %s.%s()", context, caller)) == NULL)
		msg = "";
	report_pcre2_err(ctx, ret, msg, "");
	WS_Reset(ctx->ws, snap);
	return fallback;
}

static inline VCL_STRING
sub(VRT_CTX, VCL_STRING restrict subject, VCL_STRING const restrict replacement,
    VCL_INT len, struct vmod_priv * const restrict priv_task,
    pcre2_code * restrict code,
    const struct match_call * const restrict match_opts,
    const char * const restrict context)
{
	pcre2_match_data *mdata;
	struct task *match_task = NULL;
	int ret;
	PCRE2_SIZE bytes;
	PCRE2_UCHAR *buf;
	char *msg;
	uintptr_t snap;

	if ((match_task = get_task(ctx, priv_task, context, ".sub()"))
	    == NULL)
		return NULL;
	mdata = pcre2_match_data_create_from_pattern(code, match_task->gctx);
	if (mdata == NULL) {
		VERRNOMEM(ctx, "initializing match data in %s.sub()", context);
		return NULL;
	}

	/*
	 * Don't need to ensure that the subject is in workspace, as we do
	 * with matches, because we won't be retrieving backrefs, and we
	 * give pcre2 the rest of the workspace to write the substitution.
	 */
	if (subject == NULL)
		subject = "";
	if (len == 0)
		len = PCRE2_ZERO_TERMINATED;
	buf = (PCRE2_UCHAR *) WS_Front(ctx->ws);
	bytes = (PCRE2_SIZE) WS_Reserve(ctx->ws, 0);
	/* XXX param for start_offset */
	ret = pcre2_substitute(code, (PCRE2_SPTR)subject, len, 0,
			       match_opts->match_options, mdata,
			       match_opts->mctx, (PCRE2_SPTR)replacement,
			       PCRE2_ZERO_TERMINATED, buf, &bytes);
	if (ret > 0) {
		WS_Release(ctx->ws, bytes + 1);
		return (VCL_STRING)buf;
	}

	WS_Release(ctx->ws, 0);
	if (ret == 0)
		return subject;

	if (ret == PCRE2_ERROR_NOMEMORY) {
		VERRNOMEM(ctx, "allocating substitution result in %s.sub()",
			  context);
		return NULL;
	}
	snap = WS_Snapshot(ctx->ws);
	if ((msg = WS_Printf(ctx->ws, "in %s.sub()", context)) == NULL)
		msg = "";
	report_pcre2_err(ctx, ret, msg, "");
	WS_Reset(ctx->ws, snap);
	return NULL;
}

VCL_VOID
vmod_regex__init(VRT_CTX, struct vmod_pcre2_regex **regexp,
		 const char *vcl_name, VCL_STRING pattern, COMPILE_OPTS)
{
	struct vmod_pcre2_regex *regex;
	pcre2_code *code;
	pcre2_compile_context *ccontext;
	uint32_t options = 0;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(regexp);
	AZ(*regexp);
	AN(vcl_name);
	if (pattern == NULL) {
		VERR(ctx, "pattern is NULL in %s constructor", vcl_name);
		return;
	}
	if ((ccontext = get_compile_opts(ctx, COMPILE_CTX_PARAMS,
					 COMPILE_FLAGS_PARAMS, &options,
					 vcl_name, " constructor")) == NULL)
		return;

	if ((code = compile(ctx, ccontext, pattern, options, have_jit,
			    vcl_name, " constructor")) == NULL)
	    return;

	ALLOC_OBJ(regex, VMOD_PCRE2_REGEX_MAGIC);
	AN(regex);
	*regexp = regex;
	regex->code = code;
	regex->vcl_name = strdup(vcl_name);
	AN(regex->vcl_name);
	pcre2_compile_context_free(ccontext);
}

VCL_VOID
vmod_regex__fini(struct vmod_pcre2_regex **regexp)
{
	struct vmod_pcre2_regex *regex;

	if (regexp == NULL || *regexp == NULL)
		return;
	regex = *regexp;
	CHECK_OBJ(regex, VMOD_PCRE2_REGEX_MAGIC);
	if (regex->vcl_name != NULL)
		free(regex->vcl_name);
	pcre2_code_free(regex->code);
	FREE_OBJ(regex);
}

VCL_BOOL
vmod_regex_match(VRT_CTX, struct vmod_pcre2_regex *regex,
		 struct vmod_priv *priv_call, struct vmod_priv *priv_task,
		 VCL_STRING subject, MATCH_OPTS)
{
	pcre2_match_data *mdata;
	struct task *match_task = NULL;
	struct vmod_priv *obj_task = NULL;
	struct match_call *match_opts;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(regex, VMOD_PCRE2_REGEX_MAGIC);
	AN(priv_task);
	AN(priv_call);

	if ((match_task = get_task(ctx, priv_task, regex->vcl_name, ".match()"))
	    == NULL)
		return 0;

	/* The match context and options are PRIV_CALL-scoped. */
	/* XXX param to decide whether these should be saved for the call */
	if (priv_call->priv != NULL)
		CAST_OBJ(match_opts, priv_call->priv,
			 VMOD_PCRE2_MATCH_CALL_MAGIC);
	else if ((match_opts = get_match_opts(ctx, priv_call, MATCH_CTX_PARAMS,
					      MATCH_FLAGS_PARAMS,
					      regex->vcl_name, ".match()"))
		 == NULL)
		return 0;

	/* The match data block is task-scoped for this object only. */
	obj_task = VRT_priv_task(ctx, regex);
	AN(obj_task);
	if (obj_task->priv == NULL) {
		mdata = pcre2_match_data_create_from_pattern(regex->code,
							     match_task->gctx);
		if (mdata == NULL) {
			VERRNOMEM(ctx, "initializing match data in "
				  "%s.match()", regex->vcl_name);
			return 0;
		}
		obj_task->priv = mdata;
		obj_task->free = NULL;
	}
	else {
		/*
		 * As with the general context above, pcre2_match_data is
		 * an incomplete type.
		 */
		WS_Assert_Allocated(ctx->ws, obj_task->priv, 0);
		mdata = obj_task->priv;
	}

	return match(ctx, regex->code, subject, len, match_opts->match_options,
		     mdata, match_opts->mctx, regex->vcl_name, ".match()");
}

VCL_STRING
vmod_regex_backref(VRT_CTX, struct vmod_pcre2_regex *regex, VCL_INT ref,
		   VCL_STRING fallback)
{
	struct vmod_priv *obj_task;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(regex, VMOD_PCRE2_REGEX_MAGIC);

	obj_task = VRT_priv_task(ctx, regex);
	AN(obj_task);
	return refer(ctx, ref, NULL, fallback, NUMBERED,
		     obj_task->priv, regex->vcl_name, "backref",
		     "**BACKREF METHOD FAILED**");
}

VCL_STRING
vmod_regex_namedref(VRT_CTX, struct vmod_pcre2_regex *regex, VCL_STRING name,
		    VCL_STRING fallback)
{
	struct vmod_priv *obj_task;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(regex, VMOD_PCRE2_REGEX_MAGIC);

	obj_task = VRT_priv_task(ctx, regex);
	AN(obj_task);
	return refer(ctx, -1, name, fallback, NAMED, obj_task->priv,
		     regex->vcl_name, "namedref", "**NAMEDREF METHOD FAILED**");
}

VCL_STRING
vmod_regex_sub(VRT_CTX, struct vmod_pcre2_regex *regex,
	       struct vmod_priv *priv_call, struct vmod_priv *priv_task,
	       VCL_STRING subject, VCL_STRING replacement, MATCH_OPTS,
	       SUB_OPTS)
{
	struct match_call *match_opts;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(regex, VMOD_PCRE2_REGEX_MAGIC);
	AN(priv_task);
	AN(priv_call);
	if (replacement == NULL) {
		VERR(ctx, "replacement is undefined in %s.sub()",
		     regex->vcl_name);
		return NULL;
	}

	if (priv_call->priv != NULL)
		CAST_OBJ(match_opts, priv_call->priv,
			 VMOD_PCRE2_MATCH_CALL_MAGIC);
	else if ((match_opts = get_match_opts(ctx, priv_call, MATCH_CTX_PARAMS,
					      MATCH_SUB_FLAGS_PARAMS,
					      regex->vcl_name, ".sub()"))
		 == NULL)
		return NULL;

	return sub(ctx, subject, replacement, len, priv_task, regex->code,
		   match_opts, regex->vcl_name);
}

/* Functional interface */

VCL_BOOL
vmod_match(VRT_CTX, struct vmod_priv *call, struct vmod_priv *task,
	   VCL_STRING pattern, VCL_STRING subject, COMPILE_OPTS, MATCHF_OPTS)
{
	struct match_call *match_call;
	pcre2_code *code;
	struct task *match_task;
	VCL_BOOL ret = 0;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(call);
	AN(task);
	if (pattern == NULL) {
		ERR(ctx, "pattern is NULL in pcre2.match()");
		return 0;
	}

	/* Compile and match options saved in PRIV_CALL scope */
	if (call->priv != NULL)
		CAST_OBJ(match_call, call->priv, VMOD_PCRE2_MATCH_CALL_MAGIC);
	else if ((match_call = get_match_opts(ctx, call, MATCH_CTX_PARAMS,
					      MATCH_FLAGS_PARAMS, "pcre2",
					      ".match()")) == NULL)
		return 0;
	if (match_call->cctx == NULL) {
		match_call->cctx
			= get_compile_opts(ctx, COMPILE_CTX_PARAMS,
					   COMPILE_FLAGS_PARAMS,
					   &match_call->compile_options,
					   "pcre2", ".match()");
		if (match_call->cctx == NULL)
			return 0;
	}

	if ((code = compile(ctx, match_call->cctx, pattern,
			    match_call->compile_options, have_jit && !no_jit,
			    "pcre2", ".match()")) == NULL)
		return 0;

	if ((match_task = get_task(ctx, task, "pcre2", ".match()")) == NULL)
		return 0;

	match_task->mdata
		= pcre2_match_data_create_from_pattern(code, match_task->gctx);
	if (match_task->mdata == NULL) {
		ERRNOMEM(ctx, "initializing match data in pcre2.match()");
		return 0;
	}
	WS_Assert_Allocated(ctx->ws, match_task->mdata, 0);

	ret = match(ctx, code, subject, len, match_call->match_options,
		    match_task->mdata, match_call->mctx, "pcre2", ".match()");

	pcre2_code_free(code);
	return ret;
}

VCL_STRING
vmod_backref(VRT_CTX, struct vmod_priv *task, VCL_INT ref, VCL_STRING fallback)
{
	struct task *match_task;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(task);

	if (fallback == NULL) {
		ERR(ctx, "in pcre2.backref(): fallback is undefined");
		return "**BACKREF FUNCTION FAILED**";
	}
	if (task->priv == NULL) {
		ERR(ctx, "pcre2.backref() called without prior match");
		return fallback;
	}
	WS_Assert_Allocated(ctx->ws, task->priv, sizeof(*match_task));
	CAST_OBJ(match_task, task->priv, VMOD_PCRE2_TASK_MAGIC);

	return refer(ctx, ref, NULL, fallback, NUMBERED, match_task->mdata,
		     "pcre2", "backref", "**BACKREF FUNCTION FAILED**");
}

VCL_STRING
vmod_namedref(VRT_CTX, struct vmod_priv *task, VCL_STRING name,
	      VCL_STRING fallback)
{
	struct task *match_task;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(task);

	if (fallback == NULL) {
		ERR(ctx, "in pcre2.namedref(): fallback is undefined");
		return "**NAMEDREF FUNCTION FAILED**";
	}
	if (task->priv == NULL) {
		ERR(ctx, "pcre2.namedref() called without prior match");
		return fallback;
	}
	WS_Assert_Allocated(ctx->ws, task->priv, sizeof(*match_task));
	CAST_OBJ(match_task, task->priv, VMOD_PCRE2_TASK_MAGIC);

	return refer(ctx, -1, name, fallback, NAMED, match_task->mdata, "pcre2",
		     "namedref", "**NAMEDREF FUNCTION FAILED**");
}

VCL_STRING
vmod_sub(VRT_CTX, struct vmod_priv *priv_call, struct vmod_priv *priv_task,
	 VCL_STRING pattern, VCL_STRING subject, VCL_STRING replacement,
	 COMPILE_OPTS, MATCHF_OPTS, SUB_OPTS)
{
	struct match_call *match_opts;
	pcre2_code *code;
	VCL_STRING ret;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(priv_task);
	AN(priv_call);
	if (pattern == NULL) {
		ERR(ctx, "pattern is undefined in pcre2.sub()");
		return NULL;
	}
	if (replacement == NULL) {
		ERR(ctx, "replacement is undefined in pcre2.sub()");
		return NULL;
	}

	if (priv_call->priv != NULL)
		CAST_OBJ(match_opts, priv_call->priv,
			 VMOD_PCRE2_MATCH_CALL_MAGIC);
	else if ((match_opts = get_match_opts(ctx, priv_call, MATCH_CTX_PARAMS,
					      MATCH_SUB_FLAGS_PARAMS, "pcre2",
					      ".sub()")) == NULL)
		return 0;
	if (match_opts->cctx == NULL) {
		match_opts->cctx
			= get_compile_opts(ctx, COMPILE_CTX_PARAMS,
					   COMPILE_FLAGS_PARAMS,
					   &match_opts->compile_options,
					   "pcre2", ".sub()");
		if (match_opts->cctx == NULL)
			return 0;
	}

	if ((code = compile(ctx, match_opts->cctx, pattern,
			    match_opts->compile_options, have_jit && !no_jit,
			    "pcre2", ".match()")) == NULL)
		return NULL;

	ret = sub(ctx, subject, replacement, len, priv_task, code, match_opts,
		  "pcre2");
	pcre2_code_free(code);
	return ret;
}

/* Functions */

VCL_STRING
vmod_version(VRT_CTX __attribute__((unused)))
{
	return VERSION;
}
