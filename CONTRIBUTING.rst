CONTRIBUTING
============

To contribute code or documentation, submit a pull request at the
`source repository website
<https://code.uplex.de/uplex-varnish/libvmod-pcre2>`_.

If you have a problem or discover a bug, you can post an `issue
<https://code.uplex.de/uplex-varnish/libvmod-pcre2/issues>`_ at
the website. You can also write to <varnish-support@uplex.de>.

For developers
--------------

The build specifies C99 conformance, all compiler warnings turned on,
and all warnings considered errors (compiler options
``-std=c99 -Werror -Wall -Wextra``).

The VMOD has been tested with both the gcc and clang compilers, and
MUST always compile and test successfully with both of them.

By default, ``CFLAGS`` is set to ``-g -O2``, so that symbols are
included in the shared library, and optimization is at level
``O2``. To change or disable these options, set ``CFLAGS`` explicitly
before calling ``make`` (it may be set to the empty string).

For development/debugging cycles, the ``configure`` option
``--enable-debugging`` is recommended (off by default). This will turn
off optimizations and function inlining, so that a debugger will step
through the code as expected.

The configure script runs a check to determine if the PCRE2 lib
against which it is built has Unicode enabled. If so, then some vtc
tests in ``src/tests`` with the suffix ``.yes`` are enabled, by
temporarily copying them to files with the ``.vtc`` ending, so that
``make check`` executes them. If not, then tests with the ``.no``
suffix are enabled. The VMOD MUST always pass all tests under both of
these conditions; so it is advisable to have the PCRE2 source
repository on your system, so that you can build it with and without
Unicode, and test the VMOD against both versions.

Following the PCRE2 test code, the configure script uses ``locale -a``
to determine if the ``fr_FR`` locale is installed. If so, it runs
tests using that locale, again by temporarily copying a vtc script
with the ``.yes`` suffix, or else the ``.no`` suffix. The VMOD MUST
always pass these tests as well, so it is advisable to enable
``fr_FR`` on your system (you may need to run a command like
``locale-gen``).

The VMOD makes no attempt to support some of the more obscure
properties of PCRE2 that are not likely to be relevant on any platform
on which Varnish runs. For example, we won't bother with EBCDIC. But
in general, the VMOD MUST NOT be developed in such a way that it
assumes or depends on the availability or unavailability of PCRE2's
build-time features. For example, it MUST be possible to build the
VMOD against a version of the library for which Unicode and JIT are
disabled, and then deploy the compiled code in an environment in which
are those features are available and can be used by the VMOD.
